<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome')                     ;
});

Auth::routes();


Route::get('/dashboard', 'DashboardController@dashboard')->name('dashboard');


Route::group(['middleware'=>['auth','result']], function(){


	//this routes is for subjects Manager

	Route::get('/subjects_list', 'ResultsController@subjects_list')->name('subjects_list');

	Route::get('/subjects_add_form', 'ResultsController@subjects_add_form')->name('subjects_add_form');

	Route::post('/subjects_add_post', 'ResultsController@subjects_add_post')->name('subjects_add_post');

	Route::get('/subjects_list_search', 'ResultsController@subjects_list_search')->name('subjects_list_search');



//this this routes is for Departments Manager

	Route::get('/departments_list', 'ResultsController@departments_list')->name('departments_list');

	Route::get('/departments_add_form', 'ResultsController@departments_add_form')->name('departments_add_form');

	Route::post('/departments_add_post', 'ResultsController@departments_add_post')->name('departments_add_post');

	Route::get('/departments_list_search', 'ResultsController@departments_list_search')->name('departments_list_search');

});
