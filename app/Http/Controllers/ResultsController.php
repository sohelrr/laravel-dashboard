<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\Department;
use DB;

class ResultsController extends Controller
{
   
//subjects section code start here

    public function subjects_list(){

    	$subjects = Subject::paginate(3);

    	return view('results.subjects_list')->with('subjects',$subjects);
    }

    public function subjects_add_form(){

    	return view('results.subjects_add_form');
    }
    public function subjects_add_post(Request $request){

    	$subjects =  new Subject();

    	$subjects->subjects =  $request->input('subject');

    	$subjects->save();

    	return redirect('/subjects_list');

    }

    public function subjects_list_search(Request $request){

    	$search = $request->get('search');

    	$subjects = DB::table('subjects')->where('subjects', 'like', '%'.$search.'%')->paginate(5);

    	return view('results.subjects_list', ['subjects' => $subjects]);

    }

//subjects section code end here

//Departments section code start here

    public function departments_list(Request $request){

        $departments = Department::paginate(2);

    	return view('results.departments_list')->with('departments',$departments);

    }


    public function departments_add_form(){

    	return view('results.departments_add_form');

    }


    public function departments_add_post(Request $request){

    	$departments = new Department();

    	$departments->departments = $request->input('department');

    	$departments->save();

    	return redirect('/departments_list');

    }

    public function departments_list_search(Request $request){

    	$search = $request->get('search');

    	$departments = DB::table('departments')->where('departments', 'like', '%'.$search.'%')->paginate('5');

    	return view('results.departments_list', ['departments' => $departments]);
    }




}
