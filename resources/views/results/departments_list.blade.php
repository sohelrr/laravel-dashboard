@extends('master')

@section('content')

<div class="container">
    
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card border-primary rounded-0" style="margin-bottom:0px;margin-top:0px;width:968px;">
                <div class="card-header p-0">

                      
                  
                  <div class="bg-info text-white py-2">
                      <div style="width:780px;float:left;overflow: hidden;margin-left: 20px">
                           <h3><i class="fa fa-book"></i> Department Manager</h3>
                      </div>
                      <div style="width:125px; overflow: hidden;margin-left:50px">
                        <h3><a class="btn btn-primary" href="/departments_add_form">+Add New</a> 
                      </div>
                    
                  </div>
                  </div>
  
                </div>
          </div>
           <div class="container mt-3">
            <div style="overflow: hidden;" class="col-md-12">
               <div style="float:right" class="col-md-6">
                <form action="/departments_list_search" method="get">
                  <div class="input-group">
                    <input type="text" name="search" class="form-control" style="margin-top: 6px;
border: 1px solid #bbb;
padding: 19px;">
                    <span class="input-group-prepend">
                      <button type="submit" class="btn btn-info">Search</button>
                    </span>
                  </div>
                </form>
              </div>
              <div class="col-md-6">
                  <p style="margin-top:20px;font-weight: bold;font-size:25px">Departments List</p>
                </div>
           </div>   
            <hr>  
            <table class="table table-hover text-center">
            <thead>
              <tr>
                <th>Departments Name</th>
                <th>ID</th>
                <th>Edit</th>
                <th>Delete</th>
               
              </tr>
            </thead>
            <tbody>
              
              @foreach($departments as $item)
              <tr>
                <td>{{$item->departments}}</td>
                <td>{{$item->id}}</td>
                <td><a href="/editform/" class="btn btn-info">Edit</a></td>
                <td><a href="/delete_data/" class="btn btn-danger">Delete</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
          
          <!--Below laravel code syntax for show pagination-->
          {!!  $departments->links(); !!}
            
          
          
          <div class="bg-primary text-white pt-1 pb-1">
            <p class="mt-4"><i style="margin-left:20px">Copyright &copy; SRIT<a href=""> SRIT </a>. All right reserved.</i> <i style="float:right;margin-right:20px">SR Result Manager | Version: 1.0</i></p>
            
            
          </div>

      </div>

  </div>
</div>


@endsection