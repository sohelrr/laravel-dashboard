@extends('master')

@section('content')

<div class="container">
    
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card border-primary rounded-0" style="margin-bottom:0px;margin-top:0px;width:968px;">
	            <div class="card-header p-0">
	                <div class="bg-info text-white text-center py-2">
	                    <h3><i class="fa fa-book"></i> Subjects Form</h3>
	                    <p>Add Subject Below</p>
	        		</div>
	                
        	<form action="{{route('subjects_add_post')}}" method="post" enctype="multipart/form-data">
        		{{csrf_field()}}

            	<div class="card border-primary rounded-0" style="margin-top:5px;margin-bottom:5px;">
                    <div class="card-header p-0">
                        <div class="bg-info text-white text-center py-2">
                            
                        </div>
                    </div>
                    <div class="card-body p-3">

                        <!--Body-->
                        <div class="form-group">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-book text-info"></i></div>
                                </div>
                                <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Subject Name" required>
                            </div>
                        </div>
                    </div>    
				</div>
			        
            	<div class="text-center">
                    <input type="submit" value="Submit" class="btn btn-info btn-block rounded-0 py-2">
                </div>
        	</form>
  
        </div>

	</div>
</div>


@endsection